<?php

namespace backend\controllers;

use backend\models\FileSystem;
use backend\models\FormImageCategory;
use Yii;
use backend\models\Category;
use backend\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /*return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        $category= Category::find()->where(['parent_id' => 0])->asArray()->all();
        $array = [
            '0' => ['label' => 'Categories', 'url' => ['index']],
        ];
        $navbar = [
            '0' => ['label' => 'Categories', 'url' => ['index']],
        ];
        return $this->render('list', ['category' => $category, 'array' => $array, 'navbar' => $navbar]);
    }


    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        /*return $this->render('view', [
            'model' => $this->findModel($id),
        ]);*/
        $category= Category::find()->where(['parent_id' => $id])->asArray()->all();
        $array = [
            'last' => ['label' => $this->findModel($id)['name']],
        ];
        $parentId = $this->findModel($id)['parent_id'];

        $navbar = [
            'first' => ['label' => 'Категорії', 'url' => Url::to(['category/index'])],
        ];
        $navbarValues = Category::find()->where(['parent_id' => $parentId])->asArray()->all();
        for($i=0;$i<sizeof($navbarValues);$i++){
            $value = [
                'label' => $navbarValues[$i]['name'], 'url' => Url::to(['category/view', 'id' => $navbarValues[$i]['category_id']])
            ];
            $navbar[$i] = $value;
        }

        $k = 1;
        if($parentId !=0) {
            $parentModel = $this->findModel($parentId);
            while ($parentId != 0) {
                $value = [
                    'label' => $parentModel['name'], 'url' => Url::to(['category/view', 'id' => $parentModel['category_id']])
                ];
                $array[$k] = $value;
                $k++;
                $parentId = $this->findModel($parentId)['parent_id'];
            }
        }

        if(sizeof($category) == 0) {
            $category =
                [
                    'item' => Category::find()->where(['category_id' => $id])->one(),
                ];
            //return $this->render('view', ['category' => $cate, 'array' => $array, 'depth' => $k, 'navbar' => $navbar]);
        }
        return $this->render('list', ['category' => $category, 'array' => $array, 'depth' => $k, 'navbar' => $navbar, 'descr'=>$this->findModel($id)['description']]);
    }


    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionFormImageCategory($id = 0)
    {
        $model = new FormImageCategory();
        $category = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->setNewImageName();
            if ($model->upload()) {
                $model->oldImageName = $category->image;
                $category->image = $model->newImageName;
                if ($category->save()){
                    FileSystem::deleteFile($model->getFolder() . $model->oldImageName);
                    return $this->redirect(['view', 'id' => $id]);
                } else {
                    FileSystem::deleteFile($model->getFolder() . $model->newImageName);
                }
            }
        }

        return $this->render('form_image_category', ['model' => $model]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
