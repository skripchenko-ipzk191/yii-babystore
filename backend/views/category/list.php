<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */

$this->title = 'BabyStore';
for($i=1; $i<=$depth; $i++){
    if($array[$i])
        $this->params['breadcrumbs'][] = $array[$i];
}
if($array['last'])
    $this->params['breadcrumbs'][] = $array['last'];
\yii\web\YiiAsset::register($this);
?>

<?php if (sizeof($navbar) !=1): ?>
<div class="col-sm-3 col-lg-2">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="<?=$navbar['first']['url'] ?>"><?php echo $navbar['first']['label']; ?> </a></li>
        <?php for($i=0; $i<sizeof($navbar)-1; $i++): ?>
            <?php if($navbar[$i]['label']==$array['last']['label']): ?>
                <li class="active"><a href="<?=$navbar[$i]['url'] ?>">
                        <?php echo $navbar[$i]['label']; ?>
                    </a></li><br/>
            <?php else: ?>
                <li><a href="<?=$navbar[$i]['url'] ?>">
                        <?php echo $navbar[$i]['label']; ?>
                    </a></li><br/>
            <?php endif; ?>
        <?php endfor; ?>
    </ul>
</div>
<?php endif; ?>

<h2>
    <?php echo $array['last']['label']; ?>
</h2>
<div>
        <?php echo $descr; ?>
    </div>

<?php if(sizeof($category)>1): ?>
<div class="container">
    <div class="row">
        <?php foreach($category as $categoryItem): ?>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title" href="<?=Url::to(['category/view', 'id' => $categoryItem['category_id']]) ?>">
                                <?php echo $categoryItem['name']; ?>
                            </a>

                    </div>
                    <div class="panel-body">
                        <?php if($categoryItem['parent_id'] == 0): ?>
                            <?= Html::img('@web/images/category/'.$categoryItem['image'], ['width' =>
                                '60%']) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
