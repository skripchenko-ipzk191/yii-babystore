<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FormImageCategory extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;
    public $oldImageName;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setNewImageName()
    {
        $this->newImageName = Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;
    }

    public function getFolder()
    {
        return Yii::getAlias('@backend') .'/web/images/category/';
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs($this->getFolder() . $this->newImageName);
            return true;
        } else {
            return false;
        }
    }
}