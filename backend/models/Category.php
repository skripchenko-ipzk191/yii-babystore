<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $category_id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property string $image
 *
 * @property CategoryProduct[] $categoryProducts
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'description', 'image'], 'required'],
            [['parent_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
        ];
    }

    /**
     * Gets query for [[CategoryProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryProducts()
    {
        return $this->hasMany(CategoryProduct::className(), ['category_id' => 'category_id']);
    }

    public function getImage()
    {
        if(empty($this->image)
            || !is_file(Yii::getAlias('@backend').'/web/images/category/'.$this->image)){
            return '/backend/web/images/placeholder.png';
        }
        return '/backend/web/images/category/' . $this->image;
    }
}
